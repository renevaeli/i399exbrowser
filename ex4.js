(function () {
    'use strict';

    var ul = document.getElementById('list');
    var button = document.getElementById('button1');
    var input = document.getElementById('input1');

    var counter = 1;
    var items = [new Item(counter++, 'item 1'), new Item(counter++, 'item 2')];

    render();

    button.onclick = function () {
        addItem(input.value);
        input.value = '';
    };

    function addItem(text) {
        items.push(new Item(counter++, text));
        render();
    }

    function deleteItem(id) {
        items = items.filter(function (each) {
            return each.id !== id;
        });
        render();
    }

    function render() {

        while (ul.hasChildNodes()){
            ul.removeChild(ul.lastChild);
        }

        items.forEach(function (each) {
            addLi(each);
        })
    }

    function Item(id, text) {
        this.id = id;
        this.text = text;
    }

    function addLi(item) {
        var li = document.createElement('li');
        li.innerText = item.text + ' ';

        var b = document.createElement('button');
        b.innerText = 'X';
        b.onclick = function () {
            deleteItem(item.id);
        };

        li.appendChild(b);

        ul.appendChild(li);
    }

})();
